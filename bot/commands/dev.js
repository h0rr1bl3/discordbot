module.exports = {
	name: 'dev',
    description: 'Информация о разработке',
	execute(message) {
        message.channel.send(`Исходный код: https://gitlab.com/h0rr1bl3/discordbot/\nCommit: ${process.env.CI_COMMIT_SHA}\nBranch: ${process.env.CI_COMMIT_REF_NAME}`);
	},
};