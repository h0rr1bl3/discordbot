from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.schema import Table
from sqlalchemy.orm import relationship

from database import Base


association_table = Table('association', Base.metadata,
    Column('server_bdid', Integer, ForeignKey('servers.bdid')),
    Column('op_bdid', Integer, ForeignKey('ops.bdid'))
)

class Server(Base):
    __tablename__ = "servers"

    bdid = Column(Integer, primary_key=True, index=True)
    id = Column(String)
    ops = relationship("Op", secondary=association_table)


class Op(Base):
    __tablename__ = "ops"

    bdid = Column(Integer, primary_key=True, index=True)
    id = Column(String)
